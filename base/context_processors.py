# -*- coding: utf-8 -*-
__author__ = 'sistemas'
from django.core.urlresolvers import reverse


def menu(request):
    items_menu = {
        'menu': [
            {'name': 'Dashboard', 'url': reverse('base:home'), 'icon': 'dashboard'},
            {'name': 'Solicitud', 'url': reverse('request:home'), 'icon': 'money'},
            {'name': 'Legalizaciones', 'url': reverse('request:legalization'), 'icon': 'thumbs-up'},
            {'name': 'Beneficiarios', 'url': reverse('beneficiary:home'), 'icon': 'users'},
        ],
        'menu_admin': [
            {'name': 'Prueba', 'url': '#', 'icon': 'dashboard'},
        ]
    }

    for item in items_menu['menu']:
        if request.path == item['url']:
            item['active'] = True

    for item in items_menu['menu_admin']:
        if request.path == item['url']:
            item['active'] = True

    return items_menu
