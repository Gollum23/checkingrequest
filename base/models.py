# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.db import models


class TimeStampUser(models.Model):
    create_at = models.DateTimeField(
        auto_now_add=True
    )
    update_at = models.DateTimeField(
        auto_now=True
    )
    by_user = models.ForeignKey(
        User,
        related_name='%(app_label)s_%(class)s_ownership'
    )

    class Meta:
        abstract = True