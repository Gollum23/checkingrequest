# -*- coding: utf-8 -*-
__author__ = 'sistemas'
from django.conf.urls import patterns, url

urlpatterns = patterns(
    'base.views',
    url(r'^$', 'home__view', name='home'),
    url(r'^salir/$', 'logout_view', name='logout')
    # url(r'^blog/', include('blog.urls')),
)
