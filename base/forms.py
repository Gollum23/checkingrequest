# -*- coding: utf-8 -*-
__author__ = 'sistemas'

from django.contrib.auth.forms import AuthenticationForm
from django import forms


class MyAuthenticationForm(AuthenticationForm):
    username = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Usuario', 'class': 'form-login__input--user'}
        )
    )
    password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={'placeholder': 'Contraseña', 'class': 'form-login__input--pass'}
        )
    )
