# -*- coding: utf-8 -*-
from django.contrib.auth import login, logout
from django.shortcuts import render, redirect

from .forms import MyAuthenticationForm


def home__view(request):
    if request.user.is_authenticated():
        return render(request, 'base--home.html')
    else:
        form = MyAuthenticationForm(request, request.POST or None)
        if form.is_valid():
            if form.get_user() is not None:
                login(request, form.get_user())
                return redirect(request.path)
        ctx = {
            'form': form
        }
        return render(request, 'base--login.html', ctx)


def logout_view(request):
    logout(request)
    return redirect('/solicitudcheques/')