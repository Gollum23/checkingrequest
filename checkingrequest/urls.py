from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns(
    '',
    # Examples:
    # url(r'^$', 'chekingrequest.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^solicitudcheques/', include('base.urls', namespace='base')),
    url(r'^solicitudcheques/solicitudes/', include('reqcheck.urls', namespace='request')),
    url(r'^solicitudcheques/beneficiarios/', include('beneficiaries.urls', namespace='beneficiary')),


    url(r'^solicitudcheques/admin/', include(admin.site.urls)),
)
