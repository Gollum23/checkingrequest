# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='AssigningEmployee',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('create_at', models.DateTimeField(auto_now_add=True)),
                ('update_at', models.DateTimeField(auto_now=True)),
                ('by_user', models.ForeignKey(related_name='employees_assigningemployee_ownership', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Asignaci\xf3n de empleado',
                'verbose_name_plural': 'Asignaci\xf3n de empleados',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Petitioner',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('create_at', models.DateTimeField(auto_now_add=True)),
                ('update_at', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=100, verbose_name='Solicitante')),
                ('by_user', models.ForeignKey(related_name='employees_petitioner_ownership', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Solicitante',
                'verbose_name_plural': 'Solicitantes',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='assigningemployee',
            name='petitioner',
            field=models.ManyToManyField(to='employees.Petitioner', verbose_name='Empleados'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='assigningemployee',
            name='user_assignment',
            field=models.ForeignKey(verbose_name='Usuario', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
    ]
