# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models
from django.utils.encoding import python_2_unicode_compatible, smart_unicode, smart_str

from base.models import TimeStampUser


@python_2_unicode_compatible
class Petitioner(TimeStampUser):
    """
    Modelo de solicitantes (Empleado que solicita el desembolso)
    """
    name = models.CharField(
        max_length=100,
        verbose_name='Solicitante'
    )

    class Meta:
        verbose_name = 'Solicitante'
        verbose_name_plural = 'Solicitantes'

    def __str__(self):
        return '%s' % smart_unicode(self.name)


@python_2_unicode_compatible
class AssigningEmployee(TimeStampUser):
    user_assignment = models.ForeignKey(
        User,
        verbose_name='Usuario'
    )
    petitioner = models.ManyToManyField(
        Petitioner,
        verbose_name='Empleados'
    )

    class Meta:
        verbose_name = 'Asignación de empleado'
        verbose_name_plural = 'Asignación de empleados'

    def __str__(self):
        return '%s' % self.user_assignment.username