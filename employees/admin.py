# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Petitioner, AssigningEmployee


@admin.register(Petitioner)
class PetitionerAdmin(admin.ModelAdmin):
    pass


@admin.register(AssigningEmployee)
class AssigningEmployeeAdmin(admin.ModelAdmin):
    filter_horizontal = ('petitioner',)

