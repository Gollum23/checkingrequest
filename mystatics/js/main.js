/**
 * Created by sistemas on 3/24/15.
 */

$(document).ready(inicio);

function inicio() {
    'use strict';
    $('.legalize').on('click', legalize);
    $('.turn').on('click', turn);
    $('.cancel' ).on('click', cancel_request);
    $('.approve').on('click', approve_request);
    $('#reset_form').on('click', reset_form);

}

function legalize(data) {
    data.preventDefault();
    $.get('legalize/'+ $(data.currentTarget).data('id'))
        .done(function(){
            window.location.reload();
    })
}

function turn(data) {
    data.preventDefault();
    $.get('turn/'+ $(data.currentTarget).data('id'))
        .done(function() {
            window.location.reload ();
        })
}

function cancel_request(data) {
    data.preventDefault();
    $.get('cancel/' + $(data.currentTarget ).data('id') )
            .done(function() {
                window.location.reload();
            })
}

function approve_request(data) {
    data.preventDefault();
    $.get('approve/' + $(data.currentTarget).data('id'))
        .done(function() {
            window.location.reload()
        })
}

function reset_form(data){
    data.preventDefault();
    window.location.href = '/solicitudcheques/solicitudes/'
}