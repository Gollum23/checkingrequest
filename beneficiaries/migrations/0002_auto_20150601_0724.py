# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('beneficiaries', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='beneficiary',
            name='name',
            field=models.CharField(max_length=100, verbose_name='Nombre'),
            preserve_default=True,
        ),
    ]
