# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Beneficiary',
            fields=[
                ('create_at', models.DateTimeField(auto_now_add=True)),
                ('update_at', models.DateTimeField(auto_now=True)),
                ('id', models.BigIntegerField(unique=True, serialize=False, verbose_name='Identificaci\xf3n', primary_key=True)),
                ('dv', models.PositiveIntegerField(blank=True, null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(9)])),
                ('name', models.CharField(max_length=100, verbose_name='Beneficiario')),
                ('by_user', models.ForeignKey(related_name='beneficiaries_beneficiary_ownership', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Beneficiario',
                'verbose_name_plural': 'Beneficiarios',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TypeIdentification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name='Tipo de identificaci\xf3n')),
            ],
            options={
                'verbose_name': 'Tipo de identificaci\xf3n',
                'verbose_name_plural': 'Tipos de identificaci\xf3n',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='beneficiary',
            name='type',
            field=models.ForeignKey(verbose_name='Tipo', to='beneficiaries.TypeIdentification'),
            preserve_default=True,
        ),
    ]
