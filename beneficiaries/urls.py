# -*- coding: utf-8 -*-
__author__ = 'sistemas'
from django.conf.urls import patterns, url

from .views import CreateBeneficiaryView, ListBeneficiaryView, EditBeneficiaryView

urlpatterns = patterns(
    'beneficiaries.views',
    url(r'^$', ListBeneficiaryView.as_view(), name='home'),
    url(r'^add/$', CreateBeneficiaryView.as_view(), name='add'),
    url(r'^edit/(?P<pk>[\d]+)/$', EditBeneficiaryView.as_view(), name='edit'),
)
