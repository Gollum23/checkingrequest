# -*- coding: utf-8 -*-
from __future__ import unicode_literals
__author__ = 'sistemas'
from django import forms

from .models import Beneficiary


class BeneficiaryForm(forms.ModelForm):
    id = forms.IntegerField(
        widget=forms.TextInput,
        label='Identificación'
    )
    dv = forms.IntegerField(
        widget=forms.TextInput(
            attrs={'class': 'input--2d'}
        ),
        required=False
    )
    name = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Nombre', 'class': 'input--full-width'}
        )
    )

    class Meta:
        model = Beneficiary
        fields = '__all__'
