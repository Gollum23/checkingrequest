# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.utils.encoding import python_2_unicode_compatible

from base.models import TimeStampUser


@python_2_unicode_compatible
class TypeIdentification(models.Model):
    name = models.CharField(
        max_length=50,
        verbose_name='Tipo de identificación'
    )

    class Meta:
        verbose_name = 'Tipo de identificación'
        verbose_name_plural = 'Tipos de identificación'

    def __str__(self):
        return '%s' % self.name


@python_2_unicode_compatible
class Beneficiary(TimeStampUser):
    type = models.ForeignKey(
        TypeIdentification,
        verbose_name='Tipo'
    )
    id = models.BigIntegerField(
        primary_key=True,
        unique=True,
        verbose_name='Identificación'
    )
    dv = models.PositiveIntegerField(
        validators=[
            MinValueValidator(0),
            MaxValueValidator(9)
        ],
        null=True,
        blank=True
    )
    name = models.CharField(
        max_length=100,
        verbose_name='Nombre'
    )

    class Meta:
        verbose_name = 'Beneficiario'
        verbose_name_plural = 'Beneficiarios'

    def __str__(self):
        return '%s' % self.name