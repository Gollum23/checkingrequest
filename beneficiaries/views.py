# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core.urlresolvers import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView

from base.mixins import LoginRequiredMixin

from .forms import BeneficiaryForm
from .models import Beneficiary


class ListBeneficiaryView(LoginRequiredMixin, ListView):
    model = Beneficiary
    template_name = 'beneficiaries--home.html'

    def get_queryset(self):
        queryset = self.model.objects.all().order_by('name')

        return queryset


class CreateBeneficiaryView(LoginRequiredMixin, CreateView):
    form_class = BeneficiaryForm
    template_name = 'beneficiaries--add-edit.html'
    success_url = reverse_lazy('beneficiary:home')


class EditBeneficiaryView(LoginRequiredMixin, UpdateView):
    model = Beneficiary
    form_class = BeneficiaryForm
    template_name = 'beneficiaries--add-edit.html'
    success_url = reverse_lazy('beneficiary:home')