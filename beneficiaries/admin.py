# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import TypeIdentification


@admin.register(TypeIdentification)
class TypeIdentificationAdmin(admin.ModelAdmin):
    pass
