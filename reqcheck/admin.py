# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Strategy, RequestType, Project, CategoryExpenditure


@admin.register(Strategy)
class StrategyAdmin(admin.ModelAdmin):
    pass


@admin.register(RequestType)
class RequestTypeAdmin(admin.ModelAdmin):
    pass


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    pass


@admin.register(CategoryExpenditure)
class CategoryExpenditureAdmin(admin.ModelAdmin):
    pass
