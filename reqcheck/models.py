# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
import hashlib
from django.db import models
from django.utils.encoding import python_2_unicode_compatible

from base.models import TimeStampUser
from beneficiaries.models import Beneficiary
from employees.models import Petitioner


@python_2_unicode_compatible
class RequestType(TimeStampUser):
    type_request = models.CharField(
        max_length=50,
        verbose_name='Tipo de solicitud'
    )

    class Meta:
        verbose_name = 'Tipo de solicitud'
        verbose_name_plural = 'Tipos de solicitud'

    def __str__(self):
        return '%s' % self.type_request


@python_2_unicode_compatible
class Strategy(TimeStampUser):
    name = models.CharField(
        max_length=50,
        verbose_name='Estrategia'
    )

    class Meta:
        verbose_name = 'Estrategia'
        verbose_name_plural = 'Estrategias'

    def __str__(self):
        return '%s' % self.name


@python_2_unicode_compatible
class Project(TimeStampUser):
    name = models.CharField(
        max_length=100,
        verbose_name='Proyecto'
    )
    active = models.BooleanField(
        verbose_name='Activo',
        default=True
    )

    class Meta:
        verbose_name = 'Proyecto'
        verbose_name_plural = 'Proyectos'

    def __str__(self):
        return '%s' % self.name


class CategoryExpenditure(TimeStampUser):
    name = models.CharField(
        max_length=50,
        verbose_name='Rubro'
    )

    class Meta:
        verbose_name = 'Rubro'
        verbose_name_plural = 'Rubros'

    def __str__(self):
        return '%s' % self.name


@python_2_unicode_compatible
class RequestCheck(TimeStampUser):
    request_type = models.ForeignKey(
        RequestType,
        verbose_name='Tipo de Solicitud'
    )
    petitioner = models.ForeignKey(
        Petitioner,
        verbose_name='Empleado que solicita'
    )
    project = models.ForeignKey(
        Project,
        verbose_name='Proyecto'
    )
    category = models.ForeignKey(
        CategoryExpenditure,
        verbose_name='Rubro contable',
    )
    strategy = models.ForeignKey(
        Strategy,
        verbose_name='Estrategia'
    )
    date_event = models.DateField(
        verbose_name='Fecha del evento',
        default=datetime.datetime.now()
    )
    beneficiary = models.ForeignKey(
        Beneficiary,
        verbose_name='Beneficiario'
    )
    description = models.TextField(
        verbose_name='Descripción de la solicitud / Especificaciones técnicas',
        help_text='Maximo 255 caracteres'
    )
    observations = models.TextField(
        verbose_name='Justificación / Observaciones',
        null=True,
        blank=True
    )
    state = models.BooleanField(
        verbose_name='Estado',
        default=True
    )
    turned = models.BooleanField(
        verbose_name='Girado',
        default=False
    )
    value = models.BigIntegerField(
        verbose_name='Valor'
    )
    securecode = models.CharField(
        max_length=255,
        verbose_name='Codigo de seguridad',
        blank=True
    )
    date_legalitation = models.DateField(
        verbose_name='Fecha de legalización',
        blank=True,
        null=True,
    )
    printed = models.BooleanField(
        verbose_name='Impreso',
        default=False
    )
    approved = models.BooleanField(
        verbose_name='Aprobado',
        default=False
    )

    class Meta:
        verbose_name = 'Solicitud de cheque'
        verbose_name_plural = 'Solicitudes de cheques'
        permissions = (('can_auth_request', 'Autorizar solicitudes'), )

    def save(self, *args, **kwargs):
        securecode_string = hashlib.md5(
            (
                str(self.user) + '-' +
                str(self.petitioner) + '-' +
                str(self.date_create) + '-' +
                str(self.value)
            ).encode('utf-8')
        ).hexdigest()
        self.securecode = securecode_string
        super(RequestCheck, self).save(*args, **kwargs)

    def __str__(self):
        return 'Solicitud No - %s' % self.id

    def user(self):
        return '%s' % self.by_user

    def date_create(self):
        return '%s' % self.create_at