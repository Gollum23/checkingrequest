# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from django.template.loader import render_to_string

from employees.models import AssigningEmployee
from .models import RequestCheck, Project, CategoryExpenditure
from beneficiaries.models import Beneficiary


class SelectWithPop(forms.Select):

    def render(self, name, *args, **kwargs):
        html = super(SelectWithPop, self).render(name, *args, **kwargs)
        popupplus = render_to_string("popupplus.html", {'field': name})

        return html + popupplus


class MultipleSelectWithPop(forms.Select):

    def render(self, name, *args, **kwargs):
        html = super(MultipleSelectWithPop, self).render(name, *args, **kwargs)
        popupplus = render_to_string("popupplus.html", {'field': name})

        return html + popupplus


class RequestForm(forms.ModelForm):
    beneficiary = forms.ModelChoiceField(
        Beneficiary.objects,
        widget=SelectWithPop,
        label='Beneficiario'
    )

    class Meta:
        model = RequestCheck
        exclude = ['securecode', 'date_legalitation', 'turned', 'printed', 'approved']

    def __init__(self, user, *args, **kwargs):
        # print user
        super(RequestForm, self).__init__(*args, **kwargs)
        # Filtro por usuario para traer solo los Solicitantes asignados al usuario
        projects = Project.objects.filter(active=True)
        if not user.is_superuser:
            assign = AssigningEmployee.objects.get(user_assignment=user).petitioner.all()
            self.fields['petitioner'].queryset = assign
        self.fields['project'].queryset = projects
        if user.id not in [1, 11, 12]:
            categories_filter = CategoryExpenditure.objects.all()[1:4]
        else:
            categories_filter = CategoryExpenditure.objects.all()
        categories = CategoryExpenditure.objects.filter(id__in=categories_filter)
        self.fields['category'].queryset = categories


class SearchForm(forms.Form):
    beneficiary = forms.CharField(
        required=False,
        label='Beneficiario '
    )
    from_date = forms.DateField(
        required=False,
        label='Desde '
    )
    to_date = forms.DateField(
        required=False,
        label='Hasta '
    )