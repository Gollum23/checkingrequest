# -*- coding: utf-8 -*-
__author__ = 'sistemas'
from django.conf.urls import patterns, url

from .views import ListRequestView, EditRequestView, ListLegalizationView, DetailRequestView, CreateRequestView, \
    ListRequestApi

urlpatterns = patterns(
    'reqcheck.views',
    url(r'^$', ListRequestView.as_view(), name='home'),
    url(r'^search/$', 'search_view', name='search'),
    url(r'^api/list_request/$', ListRequestApi.as_view(), name='api_home'),
    url(r'^add/$', CreateRequestView.as_view(), name='add'),
    url(r'^edit/(?P<pk>[\d]+)/$', EditRequestView.as_view(), name='edit'),
    url(r'^detail/(?P<pk>[\d]+)/$', DetailRequestView.as_view(), name='detail'),
    url(r'^legalizaciones/$', ListLegalizationView.as_view(), name='legalization'),
    url(r'^legalizaciones/legalize/(?P<req>[\d]+)/$', 'legalize_request', name='legalize'),
    url(r'^turn/(?P<req>[\d]+)/$', 'turn_request', name='turn'),
    url(r'^cancel/(?P<req>[\d]+)/$', 'cancel_request', name='cancel'),
    url(r'^print/(?P<reqnumber>[\d]+)/$', 'print_request', name='print'),
    url(r'^approve/(?P<req>[\d]+)/$', 'approve_request', name='approve'),
    url(r'^add/beneficiary/?$', 'add_beneficiary', name='add_beneficiary'),
)
