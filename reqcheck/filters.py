# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from django_filters import FilterSet
from django_filters import DateFilter
from .models import RequestCheck


class CheckingRequestFilter(FilterSet):

    class Meta:
        model = RequestCheck
        fields = {'date_event': ['gte', 'lte']}
        widget = {'date_event': DateFilter}