# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('reqcheck', '0003_auto_20150320_0755'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='active',
            field=models.BooleanField(default=True, verbose_name='Activo'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='requestcheck',
            name='date_event',
            field=models.DateField(default=datetime.datetime(2015, 3, 20, 8, 14, 54, 734404), verbose_name='Fecha del evento'),
            preserve_default=True,
        ),
    ]
