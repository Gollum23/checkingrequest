# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import froala_editor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('reqcheck', '0005_auto_20150320_0815'),
    ]

    operations = [
        migrations.AddField(
            model_name='requestcheck',
            name='printed',
            field=models.BooleanField(default=False, verbose_name='Impreso'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='requestcheck',
            name='category',
            field=models.ForeignKey(verbose_name='Rubro', blank=True, to='reqcheck.CategoryExpenditure', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='requestcheck',
            name='date_event',
            field=models.DateField(default=datetime.datetime(2015, 3, 26, 15, 25, 21, 921224), verbose_name='Fecha del evento'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='requestcheck',
            name='observations',
            field=froala_editor.fields.FroalaField(null=True, verbose_name='Observaciones', blank=True),
            preserve_default=True,
        ),
    ]
