# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import froala_editor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('reqcheck', '0002_auto_20150317_1228'),
    ]

    operations = [
        migrations.AlterField(
            model_name='requestcheck',
            name='date_event',
            field=models.DateField(default=datetime.datetime(2015, 3, 20, 7, 55, 45, 304929), verbose_name='Fecha del evento'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='requestcheck',
            name='observations',
            field=froala_editor.fields.FroalaField(null=True, verbose_name='Observaciones'),
            preserve_default=True,
        ),
    ]
