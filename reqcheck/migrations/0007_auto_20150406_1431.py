# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('reqcheck', '0006_auto_20150326_1525'),
    ]

    operations = [
        migrations.AlterField(
            model_name='requestcheck',
            name='date_event',
            field=models.DateField(default=datetime.datetime(2015, 4, 6, 14, 31, 8, 722702), verbose_name='Fecha del evento'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='requestcheck',
            name='description',
            field=models.TextField(help_text='Maximo 255 caracteres', verbose_name='Descripci\xf3n de la solicitud'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='requestcheck',
            name='observations',
            field=models.TextField(null=True, verbose_name='Observaciones', blank=True),
            preserve_default=True,
        ),
    ]
