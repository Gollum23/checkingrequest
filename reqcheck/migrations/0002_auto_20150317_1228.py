# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import datetime


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('reqcheck', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CategoryExpenditure',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('create_at', models.DateTimeField(auto_now_add=True)),
                ('update_at', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=50, verbose_name='Rubro')),
                ('by_user', models.ForeignKey(related_name='reqcheck_categoryexpenditure_ownership', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Rubro',
                'verbose_name_plural': 'Rubros',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('create_at', models.DateTimeField(auto_now_add=True)),
                ('update_at', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=100, verbose_name='Proyecto')),
                ('by_user', models.ForeignKey(related_name='reqcheck_project_ownership', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Proyecto',
                'verbose_name_plural': 'Proyectos',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='requestcheck',
            name='category',
            field=models.ForeignKey(verbose_name='Rubro', to='reqcheck.CategoryExpenditure', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='requestcheck',
            name='project',
            field=models.ForeignKey(default='', verbose_name='Proyecto', to='reqcheck.Project'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='requestcheck',
            name='date_event',
            field=models.DateField(default=datetime.datetime(2015, 3, 17, 12, 28, 22, 453624), verbose_name='Fecha del evento'),
            preserve_default=True,
        ),
    ]
