# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import froala_editor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('reqcheck', '0004_auto_20150320_0814'),
    ]

    operations = [
        migrations.AlterField(
            model_name='requestcheck',
            name='date_event',
            field=models.DateField(default=datetime.datetime(2015, 3, 20, 8, 15, 59, 901617), verbose_name='Fecha del evento'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='requestcheck',
            name='description',
            field=froala_editor.fields.FroalaField(help_text='Maximo 255 caracteres', verbose_name='Descripci\xf3n de la solicitud'),
            preserve_default=True,
        ),
    ]
