# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('reqcheck', '0009_auto_20150601_0724'),
    ]

    operations = [
        migrations.AddField(
            model_name='requestcheck',
            name='approved',
            field=models.BooleanField(default=False, verbose_name='Aprobado'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='requestcheck',
            name='date_event',
            field=models.DateField(default=datetime.datetime(2015, 6, 1, 7, 44, 22, 977001), verbose_name='Fecha del evento'),
            preserve_default=True,
        ),
    ]
