# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings
import froala_editor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('beneficiaries', '0001_initial'),
        ('employees', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='RequestCheck',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('create_at', models.DateTimeField(auto_now_add=True)),
                ('update_at', models.DateTimeField(auto_now=True)),
                ('date_event', models.DateField(default=datetime.datetime(2015, 3, 17, 10, 9, 53, 636936), verbose_name='Fecha del evento')),
                ('description', models.CharField(help_text='Maximo 255 caracteres', max_length=255, verbose_name='Descripci\xf3n de la solicitud')),
                ('observations', froala_editor.fields.FroalaField(verbose_name='Observaciones')),
                ('state', models.BooleanField(default=True, verbose_name='Estado')),
                ('turned', models.BooleanField(default=False, verbose_name='Girado')),
                ('value', models.BigIntegerField(verbose_name='Valor')),
                ('securecode', models.CharField(max_length=255, verbose_name='Codigo de seguridad', blank=True)),
                ('date_legalitation', models.DateField(null=True, verbose_name='Fecha de legalizaci\xf3n', blank=True)),
                ('beneficiary', models.ForeignKey(verbose_name='Beneficiario', to='beneficiaries.Beneficiary')),
                ('by_user', models.ForeignKey(related_name='reqcheck_requestcheck_ownership', to=settings.AUTH_USER_MODEL)),
                ('petitioner', models.ForeignKey(verbose_name='Empleado que solicita', to='employees.Petitioner')),
            ],
            options={
                'verbose_name': 'Solicitud de cheque',
                'verbose_name_plural': 'Solicitudes de cheques',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RequestType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('create_at', models.DateTimeField(auto_now_add=True)),
                ('update_at', models.DateTimeField(auto_now=True)),
                ('type_request', models.CharField(max_length=50, verbose_name='Tipo de solicitud')),
                ('by_user', models.ForeignKey(related_name='reqcheck_requesttype_ownership', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Tipo de solicitud',
                'verbose_name_plural': 'Tipos de solicitud',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Strategy',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('create_at', models.DateTimeField(auto_now_add=True)),
                ('update_at', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=50, verbose_name='Estrategia')),
                ('by_user', models.ForeignKey(related_name='reqcheck_strategy_ownership', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Estrategia',
                'verbose_name_plural': 'Estrategias',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='requestcheck',
            name='request_type',
            field=models.ForeignKey(verbose_name='Tipo de Solicitud', to='reqcheck.RequestType'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='requestcheck',
            name='strategy',
            field=models.ForeignKey(verbose_name='Estrategia', to='reqcheck.Strategy'),
            preserve_default=True,
        ),
    ]
