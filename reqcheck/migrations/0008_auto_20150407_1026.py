# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('reqcheck', '0007_auto_20150406_1431'),
    ]

    operations = [
        migrations.AlterField(
            model_name='requestcheck',
            name='category',
            field=models.ForeignKey(default=8, verbose_name='Rubro contable', to='reqcheck.CategoryExpenditure'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='requestcheck',
            name='date_event',
            field=models.DateField(default=datetime.datetime(2015, 4, 7, 10, 26, 8, 227288), verbose_name='Fecha del evento'),
            preserve_default=True,
        ),
    ]
