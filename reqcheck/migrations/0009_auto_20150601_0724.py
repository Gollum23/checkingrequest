# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('reqcheck', '0008_auto_20150407_1026'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='requestcheck',
            options={'verbose_name': 'Solicitud de cheque', 'verbose_name_plural': 'Solicitudes de cheques', 'permissions': (('can_auth_request', 'Autorizar solicitudes'),)},
        ),
        migrations.AlterField(
            model_name='requestcheck',
            name='date_event',
            field=models.DateField(default=datetime.datetime(2015, 6, 1, 7, 24, 33, 59392), verbose_name='Fecha del evento'),
            preserve_default=True,
        ),
    ]
