# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import datetime
from django import forms
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect, HttpResponse
from django.http import JsonResponse
from django.db.models import Q
from django.shortcuts import render
from django.utils.html import escape
from django.views.generic import CreateView, ListView, UpdateView, DetailView

from base.mixins import LoginRequiredMixin

from filters import CheckingRequestFilter
from .forms import RequestForm, SearchForm
from .models import RequestCheck
from beneficiaries.forms import BeneficiaryForm


class ListRequestView(LoginRequiredMixin, ListView):
    model = RequestCheck
    template_name = 'request--home.html'
    paginate_by = 12

    def get_queryset(self):
        """
        Filtro de solicitudes si el usuario esta autenticado y es del staff o super user
        trae todas las solicitudes, si es un usuario normal trae solo las solicitudes
        realizadas por el.
        """
        if self.request.user.is_authenticated():
            if self.request.user.is_staff:
                queryset = self.model.objects.all().order_by('-id')
            else:
                queryset = self.model.objects.filter(by_user=self.request.user).order_by('-id')
        else:
            queryset = super(ListRequestView, self).get_queryset()

        return queryset


class ListRequestApi(LoginRequiredMixin, ListView):
    model = RequestCheck
    paginate_by = 12

    def get(self, request, *args, **kwargs):
        self.object_list = self.get_queryset()
        return self.json_to_response()

    def json_to_response(self):
        data = [{
            'id': req.id,
            'petitioner': req.petitioner.name,
            'beneficiary': req.beneficiary.name,
            'value': req.value,
            'create_at': req.create_at,
            'project': req.project.name,
            'category': req.category.name
        } for req in self.object_list]
        return JsonResponse(data, safe=False)

    def get_queryset(self):
        """
        Filtro de solicitudes si el usuario esta autenticado y es del staff o super user
        trae todas las solicitudes, si es un usuario normal trae solo las solicitudes
        realizadas por el.
        """
        if self.request.user.is_authenticated():
            if self.request.user.is_staff:
                queryset = self.model.objects.all().order_by('-id')
            else:
                queryset = self.model.objects.filter(by_user=self.request.user).order_by('-id')
        else:
            queryset = super(ListRequestView, self).get_queryset()

        return queryset


def search_view(request):
    queryset = None
    form = SearchForm(request.POST or None)
    fmt_dates = '%d/%m/%Y'
    if form.is_valid():
        beneficiary = request.POST.get('beneficiary', False)
        from_date = request.POST.get('from_date', False)
        to_date = request.POST.get('to_date', False)
        if from_date:
            from_date = datetime.datetime.strptime(from_date, fmt_dates)
        if to_date:
            to_date = datetime.datetime.strptime(to_date, fmt_dates)
        if request.user.is_authenticated():
            if request.user.is_staff:
                if beneficiary:
                    if from_date:
                        if to_date:
                            queryset = RequestCheck.objects.filter(
                                Q(beneficiary__name__icontains=beneficiary) &
                                Q(date_event__gte=from_date) &
                                Q(date_event__lte=to_date)
                            ).order_by('-id')
                        else:
                            queryset = RequestCheck.objects.filter(
                                Q(beneficiary__name__icontains=beneficiary) &
                                Q(date_event__gte=from_date)
                            ).order_by('-id')
                    else:
                        queryset = RequestCheck.objects.filter(
                            Q(beneficiary__name__icontains=beneficiary)
                        ).order_by('-id')
                else:
                    if from_date:
                        if to_date:
                            queryset = RequestCheck.objects.filter(
                                Q(date_event__gte=from_date) &
                                Q(date_event__lte=to_date)
                            ).order_by('-id')
                        else:
                            queryset = RequestCheck.objects.filter(
                                Q(date_event__gte=from_date)
                            ).order_by('-id')
            else:
                if beneficiary:
                    if from_date:
                        if to_date:
                            queryset = RequestCheck.objects.filter(
                                Q(beneficiary__name__icontains=beneficiary) &
                                Q(date_event__gte=from_date) &
                                Q(date_event__lte=to_date),
                                by_user=request.user,
                            ).order_by('-id')
                        else:
                            queryset = RequestCheck.objects.filter(
                                Q(beneficiary__name__icontains=beneficiary) &
                                Q(date_event__gte=from_date),
                                by_user=request.user,
                            ).order_by('-id')
                    else:
                        queryset = RequestCheck.objects.filter(
                            Q(beneficiary__name__icontains=beneficiary),
                            by_user=request.user,
                        ).order_by('-id')
                else:
                    if from_date:
                        if to_date:
                            queryset = RequestCheck.objects.filter(
                                Q(date_event__gte=from_date) &
                                Q(date_event__lte=to_date),
                                by_user=request.user,
                            ).order_by('-id')
                        else:
                            queryset = RequestCheck.objects.filter(
                                Q(date_event__gte=from_date),
                                by_user=request.user,
                            ).order_by('-id')
    ctx = {
        'form': form,
        'queryset': queryset,
    }
    return render(request, 'request--search.html', ctx)


class DetailRequestView(LoginRequiredMixin, DetailView):
    model = RequestCheck
    template_name = 'request--detail.html'


class CreateRequestView(LoginRequiredMixin, CreateView):
    form_class = RequestForm
    template_name = 'request--add.html'
    success_url = reverse_lazy('request:home')

    def get(self, request, *args, **kwargs):
        form = self.form_class(self.request.user)
        ctx = {
            'form': form
        }
        return render(request, self.template_name, ctx)

    def post(self, request, *args, **kwargs):
        form = self.form_class(self.request.user, request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse_lazy('request:home'))
        ctx = {
            'form': form
        }
        return render(request, self.template_name, ctx)


class EditRequestView(LoginRequiredMixin, UpdateView):
    model = RequestCheck
    form_class = RequestForm
    template_name = 'request--edit.html'
    success_url = reverse_lazy('request:home')

    def get_object(self, queryset=None, **kwargs):
        return self.model.objects.get(pk=kwargs['pk'])

    def get(self, request, *args, **kwargs):
        form = self.form_class(self.request.user, instance=self.get_object(pk=kwargs['pk']))
        ctx = {
            'form': form
        }
        return render(request, self.template_name, ctx)

    def post(self, request, *args, **kwargs):
        form = self.form_class(self.request.user, request.POST, instance=self.get_object(pk=kwargs['pk']))
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse_lazy('request:home'))
        ctx = {
            'form': form
        }
        return render(request, self.template_name, ctx)


class ListLegalizationView(LoginRequiredMixin, ListView):
    model = RequestCheck
    template_name = 'legalization--home.html'
    paginate_by = 12

    def get_queryset(self):
        """
        Filtro de solicitudes si el usuario esta autenticado y es del staff o super user
        trae todas las solicitudes, si es un usuario normal trae solo las solicitudes
        realizadas por el.
        """
        if self.request.user.is_authenticated():
            if self.request.user.is_staff:
                queryset = self.model.objects.filter(date_legalitation=None, state=True, turned=True).order_by('-id')
            else:
                queryset = self.model.objects.filter(by_user=self.request.user, date_legalitation=None, state=True,
                                                     turned=True).order_by('-id')
        else:
            queryset = super(ListLegalizationView, self).get_queryset()

        return queryset


@login_required(login_url='/')
def legalize_request(request, req):
    if request.is_ajax():
        req = RequestCheck.objects.get(pk=req)
        req.date_legalitation = datetime.datetime.now().date()
        req.save()
        return HttpResponse()


@login_required(login_url='/')
def turn_request(request, req):
    if request.is_ajax():
        req = RequestCheck.objects.get(pk=req)
        req.turned = True
        req.save()
        return HttpResponse()


@login_required(login_url='/')
def cancel_request(request, req):
    if request.is_ajax():
        req = RequestCheck.objects.get(pk=req)
        req.state = False
        req.save()
        return HttpResponse()


@login_required(login_url='/')
def approve_request(request, req):
    if request.is_ajax():
        req = RequestCheck.objects.get(pk=req)
        req.approved = True
        req.save()
        return HttpResponse()


def handle_pop_up_add(request, add_form, field):
    form = add_form(request.POST or None)
    if form.is_valid():
        try:
            new_object = form.save()
        except forms.ValidationError:
            new_object = None
        if new_object:
            return HttpResponse(
                '<script type="text/javascript">opener.dismissAddAnotherPopup(window, "%s", "%s");</script>' %
                (escape(new_object._get_pk_val()), escape(new_object)))
    ctx = {
        'form': form,
        'field': field
    }
    return render(request, 'popadd.html', ctx)


@login_required(login_url='/')
def add_beneficiary(request):
    return handle_pop_up_add(request, BeneficiaryForm, 'beneficiary')


@login_required(login_url='/')
def print_request(request, reqnumber):
    import os
    import re
    from reportlab.lib.units import inch
    from reportlab.platypus import Paragraph, Spacer, SimpleDocTemplate
    from reportlab.lib.styles import ParagraphStyle
    from reportlab.lib.enums import TA_CENTER

    PROJECT_PATH = os.path.dirname(os.path.dirname(__file__))

    styleTitle = ParagraphStyle('defaults')
    styleTitle.alignment = TA_CENTER
    styleTitle.fontSize = 22
    styleTitle.leading = 24

    styleNormal = ParagraphStyle('defaults')
    styleNormal.fontSize = 18

    styleDescription = ParagraphStyle('defaults')
    styleDescription.fontSize = 18
    styleDescription.leading = 20

    styleObservations = ParagraphStyle('defaults')
    styleObservations.fontSize = 16
    styleObservations.leading = 18

    response = HttpResponse(content_type="application/pdf")
    response['Content-Disposition'] = 'attachment; filename="solicitud_' + reqnumber + '.pdf"'

    # Lista para los items a ubicar
    story = []

    req = RequestCheck.objects.get(pk=reqnumber)
    req.printed = True
    req.save()
    logo = os.path.join(PROJECT_PATH, 'mystatics/images/LogoViva.png')
    fuera_de_tiempo = '*********************************************************************'\
        '<br/>Este cheque fue solicitado fuera del tiempo estipulado<br/><br/>'\
        '*********************************************************************'

    def drawTemplate(canvas, doc):
        canvas.saveState()
        canvas.drawImage(logo, 0.8*inch, 10.5*inch, width=1*inch, height=0.51*inch)
        title = Paragraph('Solicitud de cheque No. ' + reqnumber, styleTitle)
        tw, th = title.wrap(6.5*inch, 1*inch)
        title.drawOn(canvas, 1*inch, 11*inch-th)
        canvas.restoreState()

    story.append(Paragraph('<b>Beneficiario:</b> ' + req.beneficiary.name, styleNormal))
    story.append(Spacer(6.5*inch, 0.4*inch))
    story.append(Paragraph('<b>Identificación:</b> %s - %s' % (
        req.beneficiary.id, req.beneficiary.dv)
        if req.beneficiary.dv else '<b>Identificación:</b> %s' % req.beneficiary.id,
        styleNormal)
    )
    story.append(Spacer(6.5*inch, 0.4*inch))
    story.append(Paragraph('<b>Descripción: </b>' + req.description, styleDescription))
    story.append(Spacer(6.5*inch, 0.4*inch))
    story.append(Paragraph(
        '<b>Valor: </b> $ %s' % re.sub("^(-?\d+)(\d{3})", '\g<1>,\g<2>', str(req.value)), styleNormal)
    )
    story.append(Spacer(6.5*inch, 0.4*inch))
    story.append(Paragraph('<b>Estrategia: </b> %s' % req.strategy.name, styleNormal))
    story.append(Spacer(6.5*inch, 0.4*inch))
    story.append(Paragraph('<b>Tipo de Solicitud: </b> %s' % req.request_type.type_request, styleNormal))
    story.append(Spacer(6.5*inch, 0.4*inch))
    story.append(Paragraph('<b>Fecha de Solicitud: </b> %s' % req.create_at.date(), styleNormal))
    story.append(Spacer(6.5*inch, 0.4*inch))
    story.append(Paragraph('<b>Fecha del Evento: </b> %s' % req.date_event, styleNormal))
    story.append(Spacer(6.5*inch, 0.4*inch))
    story.append(Paragraph(
        '<b>Observaciones: </b> %s' % req.observations if req.observations else '<b>Observaciones: </b>',
        styleObservations)
    )
    story.append(Spacer(6.5*inch, 0.4*inch))
    story.append(Paragraph('<b>Usuario que solicita: </b> %s' % req.by_user.get_full_name(), styleNormal))
    story.append(Spacer(6.5*inch, 0.4*inch))
    story.append(Paragraph('<b>Empleado que solicita: </b> %s' % req.petitioner.name, styleNormal))
    story.append(Spacer(6.5*inch, 0.4*inch))
    story.append(Paragraph('<b>Código de seguridad: </b> %s' % req.securecode, styleDescription))
    story.append(Spacer(6.5*inch, 0.4*inch))
    story.append(Paragraph(
        '%s' % fuera_de_tiempo if (req.date_event - req.create_at.date()).days < 7 else '', styleNormal)
    )

    doc = SimpleDocTemplate(response, topMargin=1.3*inch, leftMargin=0.1*inch)
    doc.build(story, onFirstPage=drawTemplate, onLaterPages=drawTemplate)
    return response
